import os
import testinfra.utils.ansible_runner

testinfra_hosts = testinfra.utils.ansible_runner.AnsibleRunner(
    os.environ['MOLECULE_INVENTORY_FILE']).get_hosts('archiver_appliance')


def test_archappl_database_tables_created(host):
    cmd = host.command('mysql -u archappl -parchappl archappl --execute="show tables"')
    assert cmd.rc == 0
    tables = cmd.stdout.split()
    # Remove header from list
    tables.remove('Tables_in_archappl')
    assert set(tables) == {'ArchivePVRequests', 'ExternalDataServers', 'PVAliases', 'PVTypeInfo'}


def test_archappl_access(archappl_index):
    assert '<title>appliance archiver - Home</title>' in archappl_index


def test_archappl_template_customization(archappl_index):
    assert '<span class="apptitle" id="archiveInstallationTitle">EPICS Archiver Appliance</span>' in archappl_index
    assert 'href="https://europeanspallationsource.se/"' in archappl_index
